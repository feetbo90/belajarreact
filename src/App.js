import './App.css';
import React, { useState } from "react";
import AmbilNilai from './AmbilNilai'

function App() {

  const [nama, setNama] = useState("")
    const [alamat, setAlamat] = useState("")
    const rubahNama = (event) => {
      setNama(event.target.value)
    }

    const rubahAlamat = (event) => {
        setAlamat(event.target.value)
    }

    console.log(`nama : ${nama}`)
    console.log(`alamat : ${alamat}`)
  return (
    <div className="App">
      <p> Masukkan Nama : </p>
      <input type="text"
             onChange={rubahNama}>
      </input>
    <p> Masukkan Alamat : </p>
    <input type="text"
           onChange={rubahAlamat}>
    </input>
        <AmbilNilai
            namaProps = {nama}
            alamatProps = {alamat}
        />
    </div>
  );
}

export default App;
